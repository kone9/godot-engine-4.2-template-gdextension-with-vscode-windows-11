#include "CuboExample.h"
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/variant/utility_functions.hpp>

using namespace godot;


CuboExample::CuboExample()
{
    if(Engine::get_singleton()->is_editor_hint()) return;
	
}

CuboExample::~CuboExample()
{

}

void CuboExample::_bind_methods()
{
    ClassDB::bind_method(D_METHOD("get_velocity_rotation"), &CuboExample::get_velocity_rotation);
    ClassDB::bind_method(D_METHOD("set_velocity_rotation", "velocityRotation"), &CuboExample::set_velocity_rotation);
    ClassDB::add_property("CuboExample", PropertyInfo(Variant::FLOAT, "velocityRotation"), "set_velocity_rotation", "get_velocity_rotation");


}

void CuboExample::_ready()
{
    if(Engine::get_singleton()->is_editor_hint()) return;
    UtilityFunctions::print("hello GDextension 3D");
    UtilityFunctions::print("hello GDextension 3D");
    UtilityFunctions::print("hello GDextension 3D");
    // UtilityFunctions::print("hola GDextension 3D");
    // UtilityFunctions::print("hola GDextension 3D");
}

void CuboExample::_process(double delta)
{
    if(Engine::get_singleton()->is_editor_hint()) return;

    rotate_y(velocityRotation * delta);

}

void CuboExample::set_velocity_rotation(const float velocity) {
	velocityRotation = velocity;
}

float CuboExample::get_velocity_rotation() const {
	return velocityRotation;
}