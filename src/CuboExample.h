#ifndef CUBOEXAMPLE_H
#define CUBOEXAMPLE_H

#pragma once

#include <godot_cpp/classes/mesh_instance3d.hpp>


namespace godot {

class CuboExample : public MeshInstance3D {
	GDCLASS(CuboExample, MeshInstance3D)

protected:
	static void _bind_methods();

public:
    CuboExample();
    ~CuboExample();

    void _process(double delta) override;

    void set_velocity_rotation(const float velocity);

    float get_velocity_rotation() const;

    void _ready() override;








private:
    float velocityRotation {1};

    Dictionary pruebaDiccionario;



};

}

#endif