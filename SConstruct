#!/usr/bin/env python
import os
import sys
from pathlib import Path

#Location of the Scontructor, which is located within the godot-cpp folder, is complemented by this.
env = SConscript("godot-cpp/SConstruct")
#Location within the godot engine project where the library will be exported.
bin_dir = "Godot_proyect/bin"


def is_file_locked(file_path):
    if not os.path.exists(file_path):
        return False
    try:
        with open(file_path, "a") as f:
            pass
    except IOError:
        return True
    return False


def msvc_pdb_rename(env, lib_full_name):
    new_name = (Path(bin_dir) / lib_full_name).as_posix()
    max_files = 256

    # clean up the old PDBs if possible
    onlyfiles = [
        f
        for f in os.listdir(Path(bin_dir))
        if os.path.isfile(os.path.join(Path(bin_dir), f)) and f.endswith(".pdb") and f.startswith(lib_full_name)
    ]
    # sort by time and leave only the newest PDB
    onlyfiles = sorted(onlyfiles, key=lambda x: os.path.getmtime(os.path.join(bin_dir, x)))[:-1]
    for of in onlyfiles:
        try:
            os.remove(Path(bin_dir) / of)
        except:
            pass

    # search for a non-blocked PDB name
    pdb_name = ""
    for s in range(max_files):
        pdb_name = "{}_{}.pdb".format(new_name, s)
        if not is_file_locked(pdb_name):
            break

    # explicit assignment of the PDB name
    env.Append(LINKFLAGS=["/PDB:" + pdb_name])


# For reference:
# - CCFLAGS are compilation flags shared between C and C++
# - CFLAGS are for C-specific compilation flags
# - CXXFLAGS are for C++-specific compilation flags
# - CPPFLAGS are for pre-processor flags
# - CPPDEFINES are for pre-processor defines
# - LINKFLAGS are for linking flags

# tweak this if you want to use different folders, or more folders, to store your source code in.
env.Append(CPPPATH=["src/"])
sources = Glob("src/*.cpp")

if env["platform"] == "macos":
    library = env.SharedLibrary(
        "{}/libgdexample.{}.{}.framework/libgdexample.{}.{}".format(
            bin_dir, env["platform"], env["target"], env["platform"], env["target"]
        ),
        source=sources,
    )
else:
    if env.get("is_msvc", False) and env["target"] != "template_release":
        msvc_pdb_rename(env, "libgdexample{}".format(env["suffix"]))

    library = env.SharedLibrary(
        "{}/libgdexample{}{}".format(bin_dir, env["suffix"], env["SHLIBSUFFIX"]),
        source=sources,
    )

Default(library)


